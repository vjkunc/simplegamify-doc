---
sidebar_position: 1
---

# Tutorial Intro

Let's discover **SimpleGamify in less than 10 minutes**.

## Getting Started

Get started by [downloading the project](https://gitlab.com/vjkunc/simplegamify) in GitLab depository.

### What you'll need

- [Docker Desktor](https://www.docker.com/products/docker-desktop/):
  - Docker is a platform for developing, packaging, and running applications using containerization technology.
- [Postman](https://www.postman.com/downloads/):
  - Postman is a collaborative API development platform that allows developers to create, test, and share APIs easily and efficiently.

## Run SimpleGamify service

After installing and running the docker desktop, run the following command in the **project root:**

```bash
docker compose up
```

This command starts the service in the docker.

Then in the command prompt of the running sg-flask container run the **following command:**

```bash
flask insert
```

This command imports demo gamification schemes for point systems, levels, actions and challenges.

That's all you needs to run the service.

## Start iterate with the API

Simply copy the following link and import it into postman:

```
{ "info": { "_postman_id": "470bc594-088c-487d-8ca2-dabdfc8575ea", "name": "SimplyGamify", "schema": "https://schema.getpostman.com/json/collection/v2.1.0/collection.json", "_exporter_id": "17100838" }, "item": [ { "name": "User", "item": [ { "name": "/user - get all users", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user", "host": [ "{{url}}" ], "path": [ "user" ] } }, "response": [] }, { "name": "/user - create user", "request": { "method": "POST", "header": [], "body": { "mode": "raw", "raw": "{\n    \"id\": \"vojtech_kunc10\",\n    \"email\": \"21\",\n    \"custom\": {\n        \"shit\": \"happens\",\n        \"balsdflasdf\": \"asdfasdflnas\"\n    }\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/user", "host": [ "{{url}}" ], "path": [ "user" ] } }, "response": [] }, { "name": "/user/:id - get user by id", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10" ] } }, "response": [] }, { "name": "/user/:id - delete user by id", "request": { "method": "DELETE", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10" ] } }, "response": [] }, { "name": "/user/:id - update user by id", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "{\n    \"email\": \"210\",\n    \"custom\": {\n        \"shit\": \"happensdd\",\n        \"balsdflasdf\": \"asdfasdflnas\"\n    }\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/user/vojtech_kunc10", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10" ] } }, "response": [] }, { "name": "/user/:id/challenge- get all user challenges", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/challenge", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "challenge" ] } }, "response": [] }, { "name": "/user/:id/challenge/:id - get user challenge", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/challenge/advanced-filler", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "challenge", "advanced-filler" ] } }, "response": [] }, { "name": "/user/:id/point - get all user points", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/point", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "point" ] } }, "response": [] }, { "name": "/user/:id/point/:id - get user point", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/point/kp", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "point", "kp" ] } }, "response": [] }, { "name": "/user/:id/action - get all user actions", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/action", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "action" ] } }, "response": [] }, { "name": "/user/:id/action/:id - get user action", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/action/comment", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "action", "comment" ] } }, "response": [] }, { "name": "/user/:id/action/:id - do action by user", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/user/vojtech_kunc10/action/comment", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "action", "comment" ] } }, "response": [] }, { "name": "/user/:id/level/:option[current/before/next] - get user level", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/user/vojtech_kunc10/level/current", "host": [ "{{url}}" ], "path": [ "user", "vojtech_kunc10", "level", "current" ] } }, "response": [] } ] }, { "name": "Leaderboard", "item": [ { "name": "/leaderboard - get leaderboard data", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/leaderboard", "host": [ "{{url}}" ], "path": [ "leaderboard" ] } }, "response": [] }, { "name": "/leaderboard_points/:point_id - get leaderboard by points type", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/leaderboard_points/xp", "host": [ "{{url}}" ], "path": [ "leaderboard_points", "xp" ] } }, "response": [] }, { "name": "/leaderboard_actions/:action_id/:option[below/above]/:number - get leaderboard by action done", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/leaderboard_actions/comment/above/6", "host": [ "{{url}}" ], "path": [ "leaderboard_actions", "comment", "above", "6" ] } }, "response": [] } ] }, { "name": "Challenge", "item": [ { "name": "/challenge- get all challenges", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/challenge", "host": [ "{{url}}" ], "path": [ "challenge" ] } }, "response": [] }, { "name": "/challenge/:id - get challenge by id", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/challenge/advanced-filler", "host": [ "{{url}}" ], "path": [ "challenge", "advanced-filler" ] } }, "response": [] }, { "name": "/challenge/:id - update challenge by id", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "{\n    \"description\": \"hahahah\"\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/challenge/advanced-filler", "host": [ "{{url}}" ], "path": [ "challenge", "advanced-filler" ] } }, "response": [] } ] }, { "name": "Action", "item": [ { "name": "/acton - get all actions", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/action", "host": [ "{{url}}" ], "path": [ "action" ] } }, "response": [] }, { "name": "/acton/:id - get action by id", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/action/comment", "host": [ "{{url}}" ], "path": [ "action", "comment" ] } }, "response": [] }, { "name": "/acton/:id - update action by id", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "{\n    \"custom\": {\n        \"some-custom-shit\": \"good comment\"\n    }\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/action/comment", "host": [ "{{url}}" ], "path": [ "action", "comment" ] } }, "response": [] } ] }, { "name": "Point", "item": [ { "name": "/point - get all points", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/point", "host": [ "{{url}}" ], "path": [ "point" ] } }, "response": [] }, { "name": "/point/:id - get point by id", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/point/xp", "host": [ "{{url}}" ], "path": [ "point", "xp" ] } }, "response": [] }, { "name": "/point/:id - update point by id", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "{\n    \"custom\": {\n        \"change\": \"seee\"\n    },\n    \"description\": \"experience posddsds\"\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/point/xp", "host": [ "{{url}}" ], "path": [ "point", "xp" ] } }, "response": [] } ] }, { "name": "Level", "item": [ { "name": "/level - get all levels", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/level", "host": [ "{{url}}" ], "path": [ "level" ] } }, "response": [] }, { "name": "/level/:id - get level by id", "request": { "method": "GET", "header": [], "url": { "raw": "{{url}}/level/level-2", "host": [ "{{url}}" ], "path": [ "level", "level-2" ] } }, "response": [] }, { "name": "/level/:id - update level by id", "request": { "method": "PUT", "header": [], "body": { "mode": "raw", "raw": "{\n    \"custom\": {\n        \"oksss\": \"youp\"\n    }\n}", "options": { "raw": { "language": "json" } } }, "url": { "raw": "{{url}}/level/level-2", "host": [ "{{url}}" ], "path": [ "level", "level-2" ] } }, "response": [] } ] } ] }

```

You should see new `SimpleGamify` collection of enpoints through which you can iterate in the demo application.

