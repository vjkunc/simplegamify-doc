---
sidebar_position: 4
---

# Actions

Actions define access points. User can get point or done challenges when performing the actions.

## Create a actions

Sctions config have to be named as `actions.yml`

Schema of the atributes allowed for actions:

```
"type" : "object",
    "additionalProperties": {
        "type" : "object",
        "properties" : {
            "desc" : {"type" : "string"},
            "points" : {"type" : "object"},
            "custom" : {"type" : "object"},
        },
    }
```

Here is a valid example:

```jsx title="simple-gamify/game-config/actions.yml"
fill-form: 
  desc: "fill form"
customize-profile:
  desc: "customize profile"
comment:
  desc: "comment form"
  points:
    kp: 5
  custom:
    something-custom: 
      first: "first"
      second: 2
```

Each key of the object is then used as ID of the action.

As you can see the points property contains the name of the points system as a key and the number of points earned after doing the action. 

## Actions API

Actions are used in next API calls:

`/action` - get all actions

`/action/:id` - get action by id

`/action/:id` - done action by id

`/user/:id/action` - get all user actions

`/user/:id/action/:id` - get user action

`/user/:id/action/:id` - **do action by user**

`/leaderboard_actions/:action_id/:option[below/above]/:number` - get leaderboard by action done