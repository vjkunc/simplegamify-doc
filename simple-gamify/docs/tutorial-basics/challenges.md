---
sidebar_position: 5
---

# Challenges

Challenges are tasks that require players to have certain skills or abilities. Rewards and points are awarded for completing challenges. 

## Create a challenges

Challenges config have to be named as `challenges.yml`

Schema of the atributes allowed for challenges:

```
"type" : "object",
    "additionalProperties": {
        "type" : "object",
        "properties" : {
            "title" : {"type" : "string"},
            "desc" : {"type" : "string"},
            "points" : {"type" : "object"},
            "challenges" : {"type" : "array"},
            "custom" : {"type" : "object"},
            "badge" : {"type" : "object"},
        },
    }
```

Here is a valid example:

```jsx title="simple-gamify/game-config/challenges.yml"
fill-5-forms:
  title: "fill 5"
  desc: "you can done this challenge by filling 5 form"
  points:
    xp: 5
  actions:
    fill-form: 5
  custom:
    badge:
      title: "begginer"
      desc: "You are doing great!"
comment-and-customize:
  desc: "comment one time and customize you profile"
  points:
    xp: 10
  actions:
    comment: 1
    customize-profile: 2
advanced-filler:
  desc: "comment 5 times and customize you profile"
  points:
    xp: 15
    rp: 50
  actions:
    fill-form: 2
  challenges: 
    - fill-5-forms
    - comment-and-customize
  custom:
    somehing: "custom"
```

Each key of the object is then used as ID of the challenge.

Just like an action, user can earn points for a challenge. The challenge is fulfilled by performing the declared number of actions and fulfilling the challenges on which it depends.

## Challenges API

Challenges are used in next API calls:

`/challenge` - get all challenges

`/challenge/:id` - get challenge by id

`/challenge/:id` - update challenge by id

`/user/:id/challenge` - get all user challenges

`/user/:id/challenge/:id` - get user challenge