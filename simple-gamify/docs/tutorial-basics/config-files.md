---
sidebar_position: 1
---

# Config files

Configuration files are written in yaml files. After saving, endpoints are made available for connection to the application.

## Add new config

To create your own gamification config when you move to `/simple-gamify/gamification-config`

SimpleGamify offers **4 types of game configs**:

- `/actions.yml` → Actions define access points after which the user receives awards, points and new levels.
- `/challenges.yml` → Challenges can be done when others actions and challenges are completed.
- `/levels.yml` → Progression through the levels is defined on the points achieved.
- `/points-systems.yml` → Many scoring systems can be defined, such as reputation points, experience points, karma points or redeemable points.

To validate and import this config to the service you need to run in sg-flask container **following command:**

```bash
flask insert
```

:::danger Take care

This command **Delete** all data saved in the service so be aware of that! Let's see how add your config to [existing one](/docs/tutorial-extras/add-config-to-existing).

:::

### Explore how to create your own configs

In next few lectures there are all the necessary knowledge to understand how to configure your own gamification.