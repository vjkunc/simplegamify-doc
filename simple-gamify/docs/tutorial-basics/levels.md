---
sidebar_position: 3
---

# Levels

Levels are usually associated with point systems, where players earn points for completing challenges and performing actions. Levels help the player see their progress.

## Create a levels

Levels config have to be named as `levels.yml`

Schema of the atributes allowed for levels:

```
"type" : "object",
    "additionalProperties": {
        "type" : "object",
        "properties" : {
            "points" : {"type" : "object"},
            "custom" : {"type" : "object"},
        },
    }
```

Here is a valid example:

```jsx title="simple-gamify/game-config/levels.yml"
level-0:
  points:
    xp: 0
level-1:
  points:
    xp: 15
    kp: 5
level-2:
  points:
    xp: 30
level-3:
  points:
    xp: 35
```

Each key of the object is then used as ID of the level.

As you can see the points property contains the name of the points system as a key and the required number of these points to reach the level. Gaining a new level is then checked each time an action is taken.

## Levels API

Levels are used in next API calls:

`/level` - get all levels

`/level/:id` - get level by id

`/level/:id` - update level by id

`/user/:id/level/:option[current/before/next]` - get user level by option current/before/next

