---
sidebar_position: 2
---

# Points systems

Points are virtual units that are used in gamification to reward players for achieving goals or completing challenges. They can be used as an indicator of success or progress in a game, or they can be used as a means of competing among players on a leaderboard.

## Create a points systems

Points system config have to be named as `points-systems.yml`

Schema of the atributes allowed for points systems:

```
"type" : "object",
    "additionalProperties": {
        "type" : "object",
        "properties" : {
            "desc" : {"type" : "string"},
            "custom" : {"type" : "object"},
        },
    }
```

Here is a valid example:

```jsx title="simple-gamify/game-config/points-systems.yml"
xp:
  desc: "description of experience points"
kp:
  desc: "description of experience points"
  custom:
    tip: "anything can be stored in custom"
rp:
  desc: "description of redeemable points"
```

Each key of the object is then used as ID of the point system.

## Points systems API

Points are used in next API calls:

`/point` - get all points systems

`/point/:id` - get point system by id

`/point/:id` - update point system by id

`/leaderboard_points/:point_id` - get leaderboard by points system

`/user/:id/point/:id` - get user point system points


