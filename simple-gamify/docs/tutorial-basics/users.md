---
sidebar_position: 6
---

# Users

Users are the only ones that are not defined in the configuration file. 

## Create a users

Users is created by post method `/user`

Schema of the atributes allowed for users:

```
"type" : "object",
    "additionalProperties": {
        "type" : "object",
        "properties" : {
            "id" : {"type" : "string"},
            "email" : {"type" : "string"},
            "username" : {"type" : "string"},
            "custom" : {"type" : "object"},
        },
    }
```

Here is a valid example for `/user` post method:

```jsx title="simple-gamify/game-config/users.yml"
{
    "id": "vojtech_kunc10",
    "email": "21",
    "custom": {
        "status": "happpy",
        "color": "gray"
    }
}
```

## Users API

Users are used in next API calls:

`/user` - get all users

`/user` - create user

`/user/:id` - get user by id

`/user/:id` - update user by id

`/user/:id` - delete user by id

`/user/:id/challenge` - get all user challenges

`/user/:id/challenge/:id` - get user challenge

`/user/:id/point` - get all user points

`/user/:id/point/:id` - get user point

`/user/:id/action` - get all user actions

`/user/:id/action/:id` - get user action

`/user/:id/action/:id` - **do action by user**

`/user/:id/level/:option[current/before/next]` - get user level