---
sidebar_position: 1
---

# Add new config to existing one

It is possible to add another configs to existing one.

For that it is required to create new folder `/simple-gamify/{folder-name}`.

Than create inside the folder **4 .yml files**:

- `/actions.yml` 
- `/challenges.yml` 
- `/levels.yml` 
- `/points-systems.yml` 

When your configs are done type to the sg-flask container **following command:**

```bash
flask add {folder-name}
```

This command adds a configuration to an existing one and links it to existing users.