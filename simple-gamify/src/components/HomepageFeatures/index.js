import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Easy to Use',
    Svg: require('@site/static/img/pacman.svg').default,
    description: (
      <>
        SimpleGamify was designed from the ground up to be easy to install and deploy using docker so you can quickly gamify your app.
      </>
    ),
  },
  {
    title: 'Focus on What Matters',
    Svg: require('@site/static/img/achivement.svg').default,
    description: (
      <>
        SimpleGamify lets you focus on designing game dynamics while we give you a tool for easy implementation. Go
        ahead and move into the <code>Tutorial</code> to learn more.
      </>
    ),
  },
  {
    title: 'Powered by Flask',
    Svg: require('@site/static/img/flask.svg').default,
    description: (
      <>
        The tool was designed as an open source web service in Flask. Feel free to modify the service and we will be happy if you point out bugs or contribute to the service.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
